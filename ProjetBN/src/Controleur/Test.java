
package Controleur;

import Modèle.*;
import Vue.*;
import java.util.Scanner;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.Serializable;
/*
import com.company.view.Joueur;

 */

public class Test implements Serializable{
    
    private static Joueur Joueur1 = null;
    private static Joueur Joueur2 = null;
    
    
    private static void displayMenu() {
        int value = 0;
        int compteur = 0;
        
        do {
                System.out.println();
                if (compteur == 1) {
                    System.out.println("SAISIE ERRONEE !");
                    System.out.println();
                }
                System.out.println("          XXXXXX    X         XXXXXXXX  XXXXXXXX  X      X          XXXXXX    XXXXXXXX  XXXXXXXX  XXXXXX    X");
                System.out.println("          X     X   X         X      X  X         X     X           X     X   X         X      X  X     X   X");
                System.out.println("          X      X  X         X      X  X         X    X            X      X  X         X      X  X      X  X");
                System.out.println("          X     X   X         X      X  X         X   X             X     X   X         X      X  X     X   X");
                System.out.println("          XXXXXX    X         XXXXXXXX  X         XXXX              XXXXXX    XXXXXXXX  XXXXXXXX  XXXXXX    X");
                System.out.println("          X     X   X         X      X  X         X   X             X         X         X      X  X   X     X");
                System.out.println("          X      X  X         X      X  X         X    X            X         X         X      X  X    X    X");
                System.out.println("          X     X   X         X      X  X         X     X           X         X         X      X  X     X   X");
                System.out.println("          XXXXXX    XXXXXXXX  X      X  XXXXXXXX  X      X          X         XXXXXXXX  X      X  X      X  XXXXXXXX");
                
                System.out.println("\n\n\n\n\n\n");
                System.out.println("[1]. JOUER");
                System.out.println("[2]. CHARGER UNE PARTIE");
                System.out.println("[3]. AIDE");
                System.out.println("[4]. QUITTER");
                System.out.println();
                System.out.print("VOTRE CHOIX : ");
                System.out.println();

                Scanner s = new Scanner(System.in);
                value = s.nextInt();
                compteur = 1;
        }while (value < 1 || value > 4);
        
        switch(value) {
            case 1 :
                /*******************************************************************************************************
                 * WE PLAY
                 ******************************************************************************************************/
                Test game = new Test();
                //return value;
                break;
            case 2 :
                /*******************************************************************************************************
                 * WE LOAD
                 ******************************************************************************************************/
                //Test game = load();

                break;
            case 3 :
                /*******************************************************************************************************
                 * HELP
                 ******************************************************************************************************/
                

                break;
            case 4 :
                /*******************************************************************************************************
                 * WE LEAVE THE GAME
                 ******************************************************************************************************/
                

                break;
        }
    }
    // METHODE POUR EFFACER LA CONSOLE/////////////////////////////////////////
        private static void Clear(){
        /*try {
            Runtime.getRuntime().exec("cls");
        } catch(Exception e){

        }*/
        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }

    private static int displayTasks() {
        int value = 0;
        int compteur = 0;

        do {
                System.out.println();
                if (compteur == 1) {
                    System.out.println("SAISIE ERRONEE !");
                    System.out.println();
                }
                System.out.println("QUE VOULEZ VOUS FAIRE?");
                System.out.println("-----------------------");
                System.out.println("[1]. CHOISIR UN BATEAU POUR ATTAQUER");
                System.out.println("[2]. VOIR VOTRE GRILLE DE BATEAUX");
                System.out.println("[3]. VOIR LA GRILLE DES RESULTATS");
                System.out.println("[4]. SAUVEGARDER ET QUITTER");
                System.out.println("");
                System.out.print("VOTRE CHOIX : ");

                Scanner s = new Scanner(System.in);
                value = s.nextInt();
                compteur = 1;
        }while (value < 1 || value > 4);

        return value;
    }

    private static Coordonnee displayAttack() {
        String[] AxeX = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
        String[] AxeY = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o"};

        String value = "";
        String X     = "";
        String Y     = "";

        int   iY = 0;

        while (true) {
            System.out.println();
            System.out.println("COORDONNEES D'ATTAQUES (EX: a6)");
            System.out.println("-------------------------------");
            System.out.println("");
            System.out.print("VOTRE CHOIX : ");

            Scanner s = new Scanner(System.in);
            value = s.next().trim();// La methode trim permet de ne pas prendre compte les espaces

            /*******************************************************************
             * Rentrer la coordonnee que l'on vise
             ******************************************************************/
            Y = String.valueOf(value.charAt(0)).toLowerCase();
            X = value.substring(1, value.length());

            int i         = 0;
            boolean found = false;

            for(i = 0; i < AxeY.length; i++) {
                if (AxeY[i].equals(Y)) {
                    iY = i;
                    found = true;
                    break;
                }
            }

            if (!found) {
                System.out.println("Coord pas correcte de a -> 0...");
                continue;
            }

            found = false;

            for(i = 0; i < AxeX.length; i++) {
                if (AxeX[i].equals(X)) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                System.out.println("Oooooooooo pas bon!!!");
                continue;
            }

            break;
        }

        System.out.println("NOUS ALLONS ATTAQUER");
        System.out.println("*************************");
        System.out.println("X:" + X + ",Y:" + Y + "(" + iY + ")");
        System.out.println("*************************");
        System.out.println("");
        System.out.println("");

        return new Coordonnee((Integer.valueOf(X) -1), iY);

    }

    private static boolean play(Joueur srcPlayer, Joueur destPalyer){
        boolean loop = false;

        //appeler la fonction qui saute des ligne
       
        System.out.println("########################################");
        System.out.println(srcPlayer.getName());
        System.out.println("########################################");

        switch (displayTasks()) {
            case 1 :
                /*******************************************************************************************************
                 * WE ATTACK
                 ******************************************************************************************************/
                Coordonnee c = displayAttack();

                switch (destPalyer.launchAttack(c)) {
                    case 0 :
                        Clear();
                        System.out.println("Louper!");
                        break;
                    case 1 :
                        Clear();
                        System.out.println("Toucher!");
                        break;
                    case 2 :
                        Clear();
                        System.out.println("Couler!");
                        break;
                    case 3 :
                        Clear();
                        System.out.println("Deja couler tarba!");
                        break;
                }

                break;
            case 2 :
                /*******************************************************************************************************
                 * WE DISPLAY PLAYGROUND
                 ******************************************************************************************************/
                srcPlayer.Grille(false);

                loop = true;

                break;
            case 3 :
                /*******************************************************************************************************
                 * WE DISPLAY OTHER PLAYER LIST
                 ******************************************************************************************************/
                destPalyer.Grille(true);

                loop = true;

                break;
            case 4 :
                /*******************************************************************************************************
                 * WE SAVE AND WE LEAVE
                 ******************************************************************************************************/
                //save(game);

                loop = true;

                break;
        }

        return loop;
    }
    
    public static void save(Test game){
		ObjectOutputStream oos = null;

	    try {
	      final FileOutputStream fichier = new FileOutputStream("binary");
	      oos = new ObjectOutputStream(fichier);
	      oos.writeObject(game);
	      oos.flush();
	    } catch (final java.io.IOException e) {
	      e.printStackTrace();
	    } finally {
	      try {
	        if (oos != null) {
	          oos.flush();
	          oos.close();
	        }
	      } catch (final IOException ex) {
	        ex.printStackTrace();
	      }
	    }
	}
	
	
	public static Test load(){
		ObjectInputStream ois = null;

		Test game = null;
		
	    try {
	      final FileInputStream fichier = new FileInputStream("binary");
	      ois = new ObjectInputStream(fichier);
	      game = (Test) ois.readObject();
	    } catch (final java.io.IOException e) {
	      e.printStackTrace();
	    } catch (final ClassNotFoundException e) {
	      e.printStackTrace();
	    } finally {
	      try {
	        if (ois != null) {
	          ois.close();
	        }
	      } catch (final IOException ex) {
	        ex.printStackTrace();
	      }
	    }
	    
		return game;
	}

    public static void main(String[] args) {
        Joueur1 = new Joueur("JEAN-PIERRE");  // GENERATE PLAYGROUND 1
        Joueur2 = new Joueur("JEREMY");  // GENERATE PLAYGROUND 2
        
        displayMenu();
        
        while (true) {
            while (true) {
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (play(Joueur1, Joueur2)) continue;
                break;
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
            }

            while (true) {
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (play(Joueur2, Joueur1)) continue;
                break;
                ////////////////////////////////////////////////////////////////////////////////////////////////////////
            }

            // TODO DEMANDER SI ON CONTINUE OU PAS???
            }
        }
        
    }

}
