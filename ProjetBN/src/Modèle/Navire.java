/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modèle;

//import java.util.ArrayList;
import java.util.Scanner;
import java.lang.Math;
import java.util.ArrayList;
import Modèle.*;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
/*
import com.company.Coord;
import com.company.view.Joueur;

/**
 *
 * @author Antoine FORYS
 */
public class Navire {
    
    private Joueur  Player;

    private int     LongueurNav;     // longueur du bateau
    private char    VisualNav;   // apparence du bateau
    private boolean Vertical; // vertical ou non

    private int     FPosX;
    private int     FPosY;

    private Coordonnee[] FCoords;       //initialisation d'un tableau vide de coordonné

    private int PositionRadom() {       //Position X et Y Random
        return ThreadLocalRandom.current().nextInt(0, 14);
    } // RANDOM POSITION BETWEEN 0 et 14

    private boolean VerticalitéRandom(){      //Verticalité Booleen Random
        return ThreadLocalRandom.current().nextInt(0, 10) > 5;
    } // RANDOM ORIENTATION

    private boolean Chevauchement(int X, int Y) {       //CHEVAUCHEMENT
        boolean found = false;

        for (int i = 0; i < this.Player.getNavire().length; i++) {
            Navire AShip = this.Player.getNavire()[i];
            if (AShip == null) continue;//objet pas instancié, pas d'erreur

            if (AShip.inCoord(X, Y) != null) {
                found = true;
                break;// un bateau a été trouvé
            }
        }

        return found;
    }

    private void generateRandomCoords() {
        int i = 0;
        int X = 0;
        int Y = 0;

        boolean found = false;

        while (true) {
            this.FCoords = new Coordonnee[LongueurNav]; //incere dans le tableau vide la taille du bateau

            this.FPosX = PositionRadom(); // RANDOM X
            this.FPosY = PositionRadom(); // RANDOM Y

            //System.out.println("Ship=[" + this.FVisual + "] X:" + FPosX + ",Y:+" + FPosY);

            if (this.Vertical) {
                X = this.FPosX;

                boolean topToBottom = (this.FPosY - this.LongueurNav < 0);// difference entre la position et la longueur du bateau

                for (i = 0; i < this.LongueurNav; i++) {
                    if (topToBottom) {
                        Y = this.FPosY + i;
                    } else {
                        Y = this.FPosY - i;
                    }

                    found = Chevauchement(X, Y);// es ce qu'il y a deux bateaux qui se croisent
                    if (found) break;

                    // FILL COORD //////////////////////////////////////////////////////////////////////////////////////
                    Coordonnee ACoord = new Coordonnee(X, Y);
                    FCoords[i] = ACoord;
                }

                if (found) {
                    continue;
                } else {
                    break; // COOL
                }

            } else {
                Y = this.FPosY;

                boolean leftTopRight = (this.FPosX - this.LongueurNav < 0);

                for (i = 0; i < this.LongueurNav; i++) {
                    if (leftTopRight) {
                        X = this.FPosX + i;
                    } else {
                        X = this.FPosX - i;
                    }

                    found = Chevauchement(X, Y);
                    if (found) break;

                    // FILL COORD //////////////////////////////////////////////////////////////////////////////////////
                    Coordonnee ACoord = new Coordonnee(X, Y);
                    FCoords[i] = ACoord;
                }

                if (found) {
                    continue;
                } else {
                    break; // COOL
                }
            } // END ELSE
        } // END WHILE
    }

    public Navire(Joueur FJoueur, int FSize, char FVisual) {
        this.LongueurNav     = FSize;
        this.VisualNav   = FVisual;
        this.Player    = FJoueur;
        this.Vertical = VerticalitéRandom();

        this.generateRandomCoords();
    }

    public Coordonnee inCoord(int X, int Y) {
        Coordonnee found = null;

        for (int i = 0; i < FCoords.length; i++) {//parcourir chaque case du chaque bateau// FCoords
            if (FCoords[i].getX() == X && FCoords[i].getY() == Y) {//
                found = FCoords[i];
                break;
            }
        }
        return found;
    }

    public char getVisual() {
        return VisualNav;
    } //

    public boolean isAlive() {
        boolean alive = false;

        for (int i = 0; i < FCoords.length; i++) {
            if (!FCoords[i].isFHit()) {
                alive = true;
                break;
            }
        }

        return alive;
    }
}

