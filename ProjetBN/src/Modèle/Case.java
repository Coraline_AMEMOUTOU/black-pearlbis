/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modèle;

/**
 *
 * @author Antoine FORYS
 */
public class Case {
    protected int col,lig;
    protected char caractereInitial = 'a';
    
    public Case(int col, int lig) {
        this.col = col;
        this.lig = lig;
    }
    
    public Case(String case1) {
        col = case1.charAt(0) - caractereInitial;
        String lig = case1.substring(1,2);
        this.lig = Integer.parseInt(lig);
    }
    
    public int getCol() {
        return col;
    }
    
    public int getLig() {
        return lig;
    }
    
    public int setCol(int col) {
        return this.col;
    }
    
    public int setLig(int lig) {
        return this.lig;
    }
    
    public void avancerNy() {
        lig++;
    }
    
    public void avancerSy() {
        lig++;
    }
    
    public void avancerOx() {
        col++;
    }
    
    public void avancerEx() {
        col++;
    }
    
    public void tournerHoraire() {
         
    }
    
    @Override
    public String toString() {
        String case1 = new String();
        case1 = case1 + (char)(col + caractereInitial) + lig;
        return case1;
    }
    
    public boolean equals(Case c) {
        return col == c.col && lig == c.lig;
    }
}
