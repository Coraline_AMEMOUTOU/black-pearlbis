/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modèle;

/**
 *
 * @author Jérémy
 */
public class Coordonnee {
    
    private int FX       = 0;
    private int FY       = 0;
    private boolean FHit = false;

    public Coordonnee(int FX, int FY) {
        this.FX   = FX;
        this.FY   = FY;

        this.FHit = false;
    }

    public int getX() {
        return FX;
    }

    public int getY() {
        return FY;
    }

    public boolean isFHit() {
        return FHit;
    }

    public void setFHit(boolean FHit) {
        this.FHit = FHit;
    }
}


