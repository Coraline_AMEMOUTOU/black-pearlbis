/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modèle;

import java.io.Serializable;
/*
import com.company.model.Ship;
import com.company.Coord;*/


public class Joueur {
    private Navire[] TabNav  = null;      // TABLEAU DE BATEAU VIDE
    private String JName   = "";        // DU NOM DU JOUEUR A VIDE

    /**
     * DRAW A SEPARATION BETWEEN A RANGE OF 15 COLUMN's
     */
    private void drawCLLine() {
        System.out.print("  ");
        for (int i = 0; i < 58; i ++) {
            System.out.print("-");
        }
        System.out.println();
    }

    public Joueur(String JName) {       // CONSTRUCTEUR DU JOUEUR
        this.JName = JName;

        TabNav = new Navire[4]; // MALLOC

        // SETUP SHIP'S
        TabNav[0] = new Navire(this, 7, 'X'); // CUIRASSE
        TabNav[1] = new Navire(this, 5, 'O'); // CROISEUR
        TabNav[2] = new Navire(this, 3, '#'); // DESTROYER
        TabNav[3] = new Navire(this, 1, '@'); // SOUS-MARIN
    }

    public void Grille(boolean onlyShowHit) {
        char[] AxeY = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o'};

        int x = 0;

        /***************************************************************************************************************
         * DRAW X LEGEND
         **************************************************************************************************************/
        System.out.print("  ┇"); // INDICES COLONNES
        for (x = 0; x < 15; x++) {
            String val = String.valueOf(x + 1);
            if (val.length() == 1) {// RAJOUT D'UN ESPACE EN HAUT DU TABLEAU
                val = " " + val;
            } else {
                val = val;
            }
            val = val + " ┇";

            System.out.print(val);
        }

        System.out.println();
        drawCLLine();

        /***************************************************************************************************************
         DRAW SHIP ELEMENTS
         **************************************************************************************************************/
        for (int y = 0; y < 15; y++) {
            System.out.print(AxeY[y] + " ┇"); // DRAW FIRST EDGE // INDICES LIGNES
            for (x = 0; x < 15; x++) {
                char colElem = ' '; // DEFAULT NOTHING, JUST A SPACE

                ////////////////////////////////////////////////////////////////////////////////////////////////////////
                /*******************************************************************************************************
                 * ARE WE DRAWING A SHIP?
                 ******************************************************************************************************/
                Navire AShip = null;
                for (int i = 0; i < TabNav.length; i++) { //construit le bateau grace à sa longueur
                    AShip = TabNav[i];// ajout d'une case sur le tableau
                    if (AShip == null) continue; // permet de ne pas renvoyer d'erreur si le tableau n'est pas instancier


                    Coordonnee ACoord = AShip.inCoord(x, y);
                    if (ACoord != null) {
                        if (onlyShowHit) {
                            // JUST IF HIT
                            if (!AShip.isAlive()) {
                                colElem = '«';
                            } else {
                                if (ACoord.isFHit()) {
                                    colElem = 'Ø';
                                }
                            }
                        } else {
                            // DISPLAY FULL BOAT MODE
                            colElem = AShip.getVisual();
                        }
                    }

                }
                ////////////////////////////////////////////////////////////////////////////////////////////////////////

                // DRAW
                System.out.print(" " + colElem + " ┇"); // DRAW ITEM ELEMENT

            }
            System.out.println();

            drawCLLine();
        }
    }

    public Navire[] getNavire() {
        return TabNav;
    }

    /*******************************************************************************************************************
     * WE LAUNCH AN ATTACK
     ******************************************************************************************************************/
    public int launchAttack(Coordonnee c) {
        int status = 0; // LOUPER
        for (int i = 0; i < this.TabNav.length; i++) {
            Navire AShip = this.TabNav[i];

            if (!AShip.isAlive()) {
                return 3;
            }

            Coordonnee ACoord = AShip.inCoord(c.getX(), c.getY());

            if (ACoord != null){
                ACoord.setFHit(true); // WE ARE KA-BOOM!!!

                if (!AShip.isAlive()) {
                    status = 2; // DEAD
                } else {
                    status = 1; // HIT
                }

                break;
            }
        }
        return status;
    }

    public String getName() {
        return JName;
    }
}
