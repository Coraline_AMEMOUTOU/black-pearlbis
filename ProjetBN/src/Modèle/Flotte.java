/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modèle;

import static java.lang.Math.random;
import java.util.ArrayList;
import java.util.*;

/**
 *
 * @author Jérémy
 */
public class Flotte {
    private ArrayList navire_vivant;
    private ArrayList navire_mort;
    
        //nb de sous ...
        //nb du cuirassé
        //nb de destroyer
        //
    int nb_de_croiseur=2;
    int nb_de_destroyer=3;
    private int nb_de_cuirassé=1;
    int nb_de_sousmarin=4;
    
    
    //verrifier que les bateau ne se tuchent pas et qu'ils se situent bien dans la map
    
    
    
    public Flotte()
    {
        
        int aleatoireX;
        int aleatoireY;
        int direction;
        navire_vivant=new ArrayList();
                
        //:::::::::::::::::::::::::::::::creation du croiseur::::::::::::::::::::::::::::::::
        for(int i=0; i<nb_de_croiseur;i++)
        {
            Croiseur croiseur;
            //do
            //{
                //On cree un cuirassee
            aleatoireX = (int) (Math.random() * (15-1) );//Pour un entier entre 1 et 15
            aleatoireY = (int) (Math.random() * (15-1) );//Pour un entier entre 1 et 15
            Random rn= new Random();
            direction= rn.nextInt(4-1+1);// chiffre aléatoire de 1 à 4
            croiseur =new Croiseur (aleatoireX, aleatoireY, direction);
            
            if (direction==1)// construit le bateau vers la gauche
            {
                for (int j=0;j<croiseur.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX-j,aleatoireY);
                    navire_vivant.add(casetmp);
                }//mettre ici la verrification de l'existance de la case
            }
            if (direction==2)//construit le bateau vers le haut
            {
                for (int j=0;j<croiseur.getlongueur();j++)
                {
                    Case casetmp;
                    casetmp = new Case(aleatoireX,aleatoireY-j);
                    navire_vivant.add(casetmp);
                }
            }
            if (direction==3)//construit le bateau vers la droite
            {
                for (int j=0;j<croiseur.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX+j,aleatoireY);
                    navire_vivant.add(casetmp);
                }
            }
            if (direction==4)//construit le bateau vers le bas
            {
                for (int j=0;j<croiseur.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX,aleatoireY+j);
                    navire_vivant.add(casetmp);
                }
            }
            for (int j=0; j<croiseur.getlongueur();j++)
                {
                Case casetmp = new Case(1, 2);
                navire_vivant.add(casetmp);
                }
            Case c=new Case(aleatoireX, aleatoireY);
            //}while(croiseur.contient(c) && croiseur.chevauche(croiseur));
            
        }
          //::::::::::::::::::::::::::::::::creation du destroyer::::::::::::::::::::::::::::
        for(int i=0; i<nb_de_destroyer;i++)
        {
            Destroyer destroyer;
           // do
           // {
                //On cree un cuirassee
            aleatoireX = (int) (Math.random() * (15-1) );//Pour un entier entre 1 et 15
            aleatoireY = (int) (Math.random() * (15-1) );//Pour un entier entre 1 et 15
            Random rn= new Random();
            direction= rn.nextInt(4-1+1);// chiffre aléatoire de 1 à 4
            destroyer =new Destroyer (aleatoireX, aleatoireY, direction);
            if (direction==1)// construit le bateau vers la gauche
            {
                for (int j=0;j<destroyer.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX-j,aleatoireY);
                    navire_vivant.add(casetmp);
                }//mettre ici la verrification de l'existance de la case
            }
            if (direction==2)//construit le bateau vers le haut
            {
                for (int j=0;j<destroyer.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX,aleatoireY-j);
                    navire_vivant.add(casetmp);
                }
            }
            if (direction==3)//construit le bateau vers la droite
            {
                for (int j=0;j<destroyer.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX+j,aleatoireY);
                    navire_vivant.add(casetmp);
                }
            }
            if (direction==4)//construit le bateau vers le bas
            {
                for (int j=0;j<destroyer.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX,aleatoireY+j);
                    navire_vivant.add(casetmp);
                }
            }
            /*
            for (int j=0; j<destroyer.getlongueur();j++)
                {
                Case casetmp = new Case(1, 2);
                navire_vivant.add(casetmp);
                }
            Case c=new Case(aleatoireX, aleatoireY);*/
          //  }while(destroyer.contient(c) && destroyer.chevauche(destroyer));
        }
        //::::::::::::::::::::::::::::::: creation du cuirassé::::::::::::::::::::::::::::::
        for(int i=0; i<nb_de_cuirassé;i++)
        {
            Cuirasse cuirasse;
            //do
            //{
                
            aleatoireX = (int) (Math.random() * (15-1) );//Pour un entier entre 1 et 15
            aleatoireY = (int) (Math.random() * (15-1) );//Pour un entier entre 1 et 15
            Random rn= new Random();
            direction= rn.nextInt(4-1+1);// chiffre aléatoire de 1 à 4
            cuirasse =new Cuirasse (aleatoireX, aleatoireY, direction);
            if (direction==1)// construit le bateau vers la gauche
            {
                for (int j=0;j<cuirasse.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX-j,aleatoireY);
                    navire_vivant.add(casetmp);
                }//mettre ici la verrification de l'existance de la case
            }
            if (direction==2)//construit le bateau vers le haut
            {
                for (int j=0;j<cuirasse.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX,aleatoireY-j);
                    navire_vivant.add(casetmp);
                }
            }
            if (direction==3)//construit le bateau vers la droite
            {
                for (int j=0;j<cuirasse.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX+j,aleatoireY);
                    navire_vivant.add(casetmp);
                }
            }
            if (direction==4)//construit le bateau vers le bas
            {
                for (int j=0;j<cuirasse.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX,aleatoireY+j);
                    navire_vivant.add(casetmp);
                }
            }
            // }while(cuirasse.contient(c) && cuirasse.chevauche());
            /*
            for (int j=0; j<cuirasse.getlongueur();j++)
                {
                Case casetmp = new Case(1, 2);//rajouter des if en fonction de la direction ou doit t'on rajouter des cases.
                navire_vivant.add(casetmp);
                }
            Case c=new Case(aleatoireX, aleatoireY);
            }while(cuirasse.contient(c) && cuirasse.chevauche(cuirasse));*/
        
        }
        
        for(int i=0; i<nb_de_sousmarin;i++)
        {
            Sousmarin sousmarin;
            //do
            //{
                //On cree un cuirassee
            aleatoireX = (int) (Math.random() * (15-1) );//Pour un entier entre 1 et 15
            aleatoireY = (int) (Math.random() * (15-1) );//Pour un entier entre 1 et 15
            Random rn= new Random();
            direction= rn.nextInt(4-1+1);// chiffre aléatoire de 1 à 4
            sousmarin =new Sousmarin (aleatoireX, aleatoireY, direction);
            if (direction==1)// construit le bateau vers la gauche
            {
                for (int j=0;j<sousmarin.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX-j,aleatoireY);
                    navire_vivant.add(casetmp);
                }//mettre ici la verrification de l'existance de la case
            }
            if (direction==2)//construit le bateau vers le haut
            {
                for (int j=0;j<sousmarin.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX,aleatoireY-j);
                    navire_vivant.add(casetmp);
                }
            }
            if (direction==3)//construit le bateau vers la droite
            {
                for (int j=0;j<sousmarin.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX+j,aleatoireY);
                    navire_vivant.add(casetmp);
                }
            }
            if (direction==4)//construit le bateau vers le bas
            {
                for (int j=0;j<sousmarin.getlongueur();j++)
                {
                    Case casetmp=new Case(aleatoireX,aleatoireY+j);
                    navire_vivant.add(casetmp);
                }
            }/*
            for (int j=0; j<sousmarin.getlongueur();j++)
                {
                Case casetmp = new Case(1, 2);
                nb_de_sousmarin.add(casetmp);
                }
            Case c=new Case(aleatoireX, aleatoireY);*/
            //}while(sousmarin.contient(c) && sousmarin.chevauche(sousmarin));
        }
        for(int i = 0; i < navire_vivant.size(); i++)
        {
            System.out.println("\n"+ navire_vivant.get(i));
        }   
        
           // }while(ArrayList.contains(Cuirasse) && );
        
        /*
        // si entre 0 et 1 c'est 
        if(aleatoireDirection < 2) {
            vertical = true;
            if(aleatoireDirection < 1) {
                nord = true;
            }
            else {
                nord = false;
            }
        }
        else {
            vertical = false;
            if(aleatoireDirection > 3) {
                est = true;
            }
            else {
                est = false;
            }
        }*
        
        }while()
        positions.add(new Position(aleatoireX,aleatoireY));
        for(
                )
    */
    }
}
    
    
    

